﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PermanenteEvalutieMocking
{
    public interface IWorldTimeApi
    {
        public int getCurrentTime();
        public string getTimeZone();
    }
    public class WorldTimeApi : IWorldTimeApi
    {
        public string _ip { get; set; }
        public int getCurrentTime()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                string url = $"http://worldtimeapi.org/api/ip/{_ip}";
                var httpResponse = httpClient.GetAsync(url).GetAwaiter().GetResult();
                var res = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var time = JsonConvert.DeserializeObject<WorldTime>(res).DateTime;
                DateTimeOffset timeOffset = DateTimeOffset.Parse(time);
                return timeOffset.Hour;
            };
        }
        public string getTimeZone()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                string url = $"http://worldtimeapi.org/api/ip/{_ip}";
                var httpResponse = httpClient.GetAsync(url).GetAwaiter().GetResult();
                var res = httpResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var timeZone = JsonConvert.DeserializeObject<WorldTime>(res).TimeZone;
                return timeZone;
            };
        }
        public string getTimeZoneParsed(string timezoneString)
        {
            if (!(timezoneString.Length == 0))
            {
                if ((timezoneString.Contains("/")))
                {
                    var timeZoneParsed = timezoneString.Split("/")[1];
                    return timeZoneParsed;
                } else
                {
                    throw new ArgumentException(nameof(timezoneString), "Timezone is not right template (must contain /)");
                }
                
            } else
            {
                throw new ArgumentException(nameof(timezoneString), "Timezone can't be empty");
            }
            
            
        }
    }
}
