﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvalutieMocking
{
    public class WorldService
    {
        private readonly IWorldTimeApi _api;

        public WorldService(IWorldTimeApi api)
        {
            _api = api;
        }

        public string GetCurrentTimeOfDay()
        {
            var time = _api.getCurrentTime();
            string returnWaarde = "Op dit IP is het momenteel ";
            if (time < 7)
            {
                return returnWaarde + "nacht.";
            }
            if (time < 12)
            {
                return returnWaarde + "voormiddag.";
            }
            if (time < 13)
            {
                return returnWaarde + "middag.";
            }
            if (time < 17)
            {
                return returnWaarde + "namiddag.";
            }
            else
            {
                return returnWaarde + "avond.";
            }
        }
        
    }
}
