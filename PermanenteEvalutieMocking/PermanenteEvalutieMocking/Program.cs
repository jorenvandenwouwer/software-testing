﻿

using System;
namespace PermanenteEvalutieMocking
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een IP address op (xxx.xxx.xxx.xxx)");
            string ip = Console.ReadLine();
            WorldTimeApi api = new WorldTimeApi();
            api._ip = ip;
            WorldService service = new WorldService(api);
            Console.WriteLine(service.GetCurrentTimeOfDay());
            OpenWeatherApi openWeatherApi = new OpenWeatherApi();
            openWeatherApi.timeZone = api.getTimeZoneParsed(api.getTimeZone());
            Console.WriteLine("Het is op deze locatie " +  openWeatherApi.getTempFromTimeZone() + " graden.");

        }
    }
}
