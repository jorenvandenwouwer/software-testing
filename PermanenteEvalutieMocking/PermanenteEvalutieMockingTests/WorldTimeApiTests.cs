﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PermanenteEvalutieMocking;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvalutieMocking.Tests
{
    [TestClass()]
    public class WorldTimeApiTests
    {
        [TestMethod()]
        public void getTimeZoneParsedTest_With_No_Header()
        {
            var worldTimeApi = new WorldTimeApi();
            var timeZoneString = "Europ/Brussels";
            var expected = "Brussels";
            var result = worldTimeApi.getTimeZoneParsed(timeZoneString);
            Assert.AreEqual(expected, result);
        }
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException),"Timezone can't be empty")]
        public void getTimeZoneParsedTest_Empty_String()
        {
            var worldTimeApi = new WorldTimeApi();
            var timeZoneString = "";
            var result = worldTimeApi.getTimeZoneParsed(timeZoneString);
            
        }
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException), "Timezone is not right template (must contain /)")]
        public void getTimeZoneParsedTest_Does_Not_Contain_Forward_Slash()
        {
            var worldTimeApi = new WorldTimeApi();
            var timeZoneString = "Testing";
            var expected = "Brussels";
            var result = worldTimeApi.getTimeZoneParsed(timeZoneString);
            Assert.AreEqual(expected, result);

        }

    }
}