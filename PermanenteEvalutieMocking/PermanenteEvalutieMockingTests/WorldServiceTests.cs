﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PermanenteEvalutieMocking;
using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvalutieMocking.Tests
{
    [TestClass()]
    public class WorldServiceTests
    {
        [TestMethod()]
        public void GetCurrentTimeTest_Returns_Nacht()
        {
            var worldtime = new Mock<IWorldTimeApi>();
            worldtime.Setup(x => x.getCurrentTime()).Returns(5);
            var service = new WorldService(worldtime.Object);
            string res = service.GetCurrentTimeOfDay();
            Assert.AreEqual(res, "Op dit IP is het momenteel nacht.");
        }
        [TestMethod()]
        public void GetCurrentTimeTest_Returns_Voormiddag()
        {
            var worldtime = new Mock<IWorldTimeApi>();
            worldtime.Setup(x => x.getCurrentTime()).Returns(8);
            var service = new WorldService(worldtime.Object);
            string res = service.GetCurrentTimeOfDay();
            Assert.AreEqual(res, "Op dit IP is het momenteel voormiddag.");
        }
        [TestMethod()]
        public void GetCurrentTimeTest_Returns_Middag()
        {
            var worldtime = new Mock<IWorldTimeApi>();
            worldtime.Setup(x => x.getCurrentTime()).Returns(12);
            var service = new WorldService(worldtime.Object);
            string res = service.GetCurrentTimeOfDay();
            Assert.AreEqual(res, "Op dit IP is het momenteel middag.");
        }
        [TestMethod()]
        public void GetCurrentTimeTest_Returns_Namiddag()
        {
            var worldtime = new Mock<IWorldTimeApi>();
            worldtime.Setup(x => x.getCurrentTime()).Returns(14);
            var service = new WorldService(worldtime.Object);
            string res = service.GetCurrentTimeOfDay();
            Assert.AreEqual(res, "Op dit IP is het momenteel namiddag.");
        }
        [TestMethod()]
        public void GetCurrentTimeTest_Returns_Avond()
        {
            var worldtime = new Mock<IWorldTimeApi>();
            worldtime.Setup(x => x.getCurrentTime()).Returns(22);
            var service = new WorldService(worldtime.Object);
            string res = service.GetCurrentTimeOfDay();
            Assert.AreEqual(res, "Op dit IP is het momenteel avond.");
        }
    }
}